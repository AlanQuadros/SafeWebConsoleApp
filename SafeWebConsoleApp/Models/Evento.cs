﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SafeWebConsoleApp.Models
{
    public class Evento
    {
        public Evento()
        {
            Dias = new List<Dia>();
        }

        public IList<Dia> Dias { get; set; }

        public bool Agendar(List<Palestra> palestras)
        {
            if (palestras.Count <= 0)
            {
                return false;
            }

            var dia = new Dia();
            Dias.Add(dia);

            palestras.ForEach(p =>
            {
                if (p.Tempo <= dia.Manha.FimTurno - dia.Manha.InicioTurno)
                {
                    p.Horario = dia.Manha.InicioTurno;
                    dia.Manha.Palestras.Add(p);
                    dia.Manha.InicioTurno += p.Tempo;

                }
                else if (p.Tempo <= dia.Tarde.FimTurno - dia.Tarde.InicioTurno)
                {
                    p.Horario = dia.Tarde.InicioTurno;
                    dia.Tarde.Palestras.Add(p);
                    dia.Tarde.InicioTurno += p.Tempo;
                }
                else
                {
                    dia = new Dia();
                    Dias.Add(dia);
                    p.Horario = dia.Manha.InicioTurno;
                    dia.Manha.Palestras.Add(p);
                    dia.Manha.InicioTurno += p.Tempo;
                }
            });
            return true;
        }

        public void ListarEventoCompleto()
        {
            var i = 1;
            Dias.ToList().ForEach(d =>
            {
                Console.WriteLine($"{i}º dia\n");

                d.PalestrasDoDia(d.Manha).ForEach(p =>
                    Console.WriteLine($"{p.Horario:hh\\:mm} {p.NomePalestrante}: {p.Titulo} {p.Tempo.TotalMinutes}min"));

                Console.WriteLine($"{d.Manha.InicioTurno:hh\\:mm} Almoço");

                d.PalestrasDoDia(d.Tarde).ForEach(p =>
                    Console.WriteLine($"{p.Horario:hh\\:mm} {p.NomePalestrante}: {p.Titulo} {p.Tempo.TotalMinutes}min"));

                Console.WriteLine(d.Tarde.InicioTurno >= TimeSpan.FromHours(16)
                    ? $"{d.Tarde.InicioTurno:hh\\:mm} Dúvidas e Debates\n"
                    : "16:00 Dúvidas e Debates\n");
                i++;
            });
        }
    }
}
