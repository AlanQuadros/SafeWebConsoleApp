﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SafeWebConsoleApp.Models
{
    public class Dia
    {
        public Dia()
        {
            Manha = new Turno
            {
                InicioTurno = TimeSpan.FromHours(9),
                FimTurno = TimeSpan.FromHours(12),
                Palestras = new List<Palestra>()
            };
            Tarde = new Turno
            {
                InicioTurno = TimeSpan.FromHours(13),
                FimTurno = TimeSpan.FromHours(17),
                Palestras = new List<Palestra>()
            };
        }

        public Turno Manha { get; set; }
        public Turno Tarde { get; set; }

        public List<Palestra> PalestrasDoDia(Turno turno)
        {
            return turno.Palestras.ToList();
        }
    }
}
