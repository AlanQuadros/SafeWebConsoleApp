﻿using System;
using System.Collections.Generic;

namespace SafeWebConsoleApp.Models
{
    public class Turno
    {
        public TimeSpan InicioTurno { get; set; }
        public TimeSpan FimTurno { get; set; }
        public IList<Palestra> Palestras { get; set; }
    }
}
