﻿using System;

namespace SafeWebConsoleApp.Models
{
    public class Palestra
    {
        public string NomePalestrante { get; set; }
        public string Titulo { get; set; }
        public TimeSpan Tempo { get; set; }
        public TimeSpan Horario { get; set; }
    }
}
