﻿using System;
using System.Collections.Generic;
using System.IO;
using SafeWebConsoleApp.Models;

namespace SafeWebConsoleApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var evento = new Evento();
            var palestras = LerArquivo("dados.txt");                   
            evento.Agendar(palestras);        
            evento.ListarEventoCompleto();                    

            Console.ReadLine();
        }

        public static List<Palestra> LerArquivo(string arquivo)
        {
            var palestras = new List<Palestra>();
            var linhas = File.ReadAllLines(arquivo);
            for (var i = 0; i < linhas.Length; i += 3)
            {
                var palestra = new Palestra
                {
                    NomePalestrante = linhas[i],
                    Titulo = linhas[i + 1],
                    Tempo = TimeSpan.FromMinutes(int.Parse(linhas[i + 2].Split('m')[0]))
                };
                palestras.Add(palestra);
            }

            return palestras;
        }
    }
}
