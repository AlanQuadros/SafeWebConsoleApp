﻿using System;
using System.Collections.Generic;
using SafeWebConsoleApp.Models;

namespace SafeWebConsoleApp
{
    public class Dados
    {
        // Não utilizado. Dados estão sendo carregados do arquivo dados.txt em "bin/Debug"
        public static List<Palestra> Palestras = new List<Palestra>
        {
            new Palestra
            {
                NomePalestrante = "Dr. Ruy Ramos",
                Titulo = "Certificado de Atributo",
                Tempo = TimeSpan.FromMinutes(60)
            },
            new Palestra
            {
                NomePalestrante = "Dr. Fabiano Menke",
                Titulo = "Diretivas da Assinatura Digital",
                Tempo = TimeSpan.FromMinutes(45)
            },
            new Palestra
            {
                NomePalestrante = "Luiz Carlos Zancanella Junior",
                Titulo = "Carimbo de Tempo",
                Tempo = TimeSpan.FromMinutes(30)
            },
            new Palestra
            {
                NomePalestrante = "Ireneu Orth",
                Titulo = "Case Tapera",
                Tempo = TimeSpan.FromMinutes(45)
            },
            new Palestra
            {
                NomePalestrante = "Dr. Renato Martini",
                Titulo = "Visão da Presidência da República",
                Tempo = TimeSpan.FromMinutes(45)
            },
            new Palestra
            {
                NomePalestrante = "Antônio Gomes",
                Titulo = "Visão do Estado do Rio Grande do Sul",
                Tempo = TimeSpan.FromMinutes(5)
            },
            new Palestra
            {
                NomePalestrante = "Famurs",
                Titulo = "Visão dos Municipios",
                Tempo = TimeSpan.FromMinutes(60)
            },
            new Palestra
            {
                NomePalestrante = "Carlos Eduardo Wagner",
                Titulo = "Certificado Digital no Banrisul",
                Tempo = TimeSpan.FromMinutes(45)
            },
            new Palestra
            {
                NomePalestrante = "Cynthia Anzanello",
                Titulo = "Certificação na administração pública estadual do RS",
                Tempo = TimeSpan.FromMinutes(30)
            },
            new Palestra
            {
                NomePalestrante = "Paulo Roberto Kopschina",
                Titulo = "Serviços da Jucergs que fazem uso do certificado digital",
                Tempo = TimeSpan.FromMinutes(30)
            },
            new Palestra
            {
                NomePalestrante = "Dr. Júlio César Fonseca",
                Titulo = "A experiência do escritório Mattos Filho com certificado",
                Tempo = TimeSpan.FromMinutes(45)
            },
            new Palestra
            {
                NomePalestrante = "André Luiz Assis",
                Titulo = "O uso do certificado ICP-Brasil no IGP-RS",
                Tempo = TimeSpan.FromMinutes(60)
            },
            new Palestra
            {
                NomePalestrante = "Pedro Paulo Lemos",
                Titulo = "Mesa Aberta",
                Tempo = TimeSpan.FromMinutes(60)
            },
            new Palestra
            {
                NomePalestrante = "Nivaldo Cleto",
                Titulo = "Apresentação AARB",
                Tempo = TimeSpan.FromMinutes(45)
            },
            new Palestra
            {
                NomePalestrante = "Regina Tupinambá",
                Titulo = "A comunicação no mundo da certificação digital",
                Tempo = TimeSpan.FromMinutes(30)
            },
            new Palestra
            {
                NomePalestrante = "Junior Fuganholi",
                Titulo = "Palestra Parcerias com as ARs –Startup Xml NFe Estadual",
                Tempo = TimeSpan.FromMinutes(30)
            },
            new Palestra
            {
                NomePalestrante = "Patrícia T Oliveira Leite",
                Titulo = "Palestra de Compliance",
                Tempo = TimeSpan.FromMinutes(60)
            },
            new Palestra
            {
                NomePalestrante = "Maurício Coelho: ICP-Brasil",
                Titulo = "Perspectivas e Números",
                Tempo = TimeSpan.FromMinutes(30)
            },
            new Palestra
            {
                NomePalestrante = "Adriano Carlos Gliorsi",
                Titulo = "Certificação Digital na saúde",
                Tempo = TimeSpan.FromMinutes(30)
            }

        };
    }
}
