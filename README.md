# SafeWebConsoleApp

* **O que vale destacar no código implementado?**

O sistema foi projeto para que possa existir um evento com um número ilimitados de dias. Dependendo apenas da necessidade de adicionar novos dias.

* **O que poderia ser feito para melhorar o sistema?**

Um algorítmo de balanceamento de horário, para melhor adaptar as palestras ao longo do dia.



## Execução do projeto

A classe Program dentro do projeto SafeWebConsoleApp é a classe principal da aplicação.
Os dados são lidos de um arquivo ".txt" presente na pasta "bin/Debug" do projeto.
A saída do sistema é feita diretamente no console.