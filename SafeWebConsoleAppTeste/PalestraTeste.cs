﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SafeWebConsoleApp.Models;

namespace SafeWebConsoleAppTeste
{
    [TestClass]
    public class PalestraTeste
    {
        [TestMethod]
        public void InserePalestraEmHorarioValiidoDaManha()
        {
            var palestra = new Palestra();
            var dia = new Dia();

            palestra.Horario = dia.Manha.InicioTurno;
            Assert.AreEqual(TimeSpan.FromHours(9), palestra.Horario);
        }

        [TestMethod]
        public void InserePalestraEmHorarioValiidoDaTarde()
        {
            var palestra = new Palestra();
            var dia = new Dia();

            palestra.Horario = dia.Tarde.InicioTurno;
            Assert.AreEqual(TimeSpan.FromHours(13), palestra.Horario);
        }

        [TestMethod]
        public void ListaPalestrasSeExistirem()
        {
            var palestras = new List<Palestra>
            {
                new Palestra()
            };
            var turno = new Turno
            {
                Palestras = palestras
            };
            var dia = new Dia
            {
                Manha = turno
            };
            Assert.IsNotNull(dia.PalestrasDoDia(turno));        
        }

        [TestMethod]
        public void CriaPalestranteDoArquivo()
        {
            var linhas = File.ReadAllLines("dados.txt");
            
            var palestra = new Palestra
            {
                NomePalestrante = linhas[0],
                Titulo = linhas[1],
                Tempo = TimeSpan.FromMinutes(int.Parse(linhas[2].Split('m')[0]))
            };
            

            Assert.AreEqual("Dr. Ruy Ramos", palestra.NomePalestrante);
        }

        [TestMethod]
        public void AgendarPalestrarSeExistirem()
        {
            var palestras = new List<Palestra>();
            var linhas = File.ReadAllLines("dados.txt");
            var palestra = new Palestra
            {
                NomePalestrante = linhas[0],
                Titulo = linhas[1],
                Tempo = TimeSpan.FromMinutes(int.Parse(linhas[2].Split('m')[0]))
            };

            palestras.Add(palestra);

            var evento = new Evento();
            Assert.IsTrue(evento.Agendar(palestras));
        }
    }
}
